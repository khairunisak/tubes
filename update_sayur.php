<?php 
	include "config.php"
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css" />

    <title>UPDATE SAYUR</title>
	<style media="screen">

  body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

  * {
    box-sizing: border-box;
  }

    .nav-item a:hover {
      background-color: #ddd;
      color: black;
    }
    .topnav a.active {
      background-color: #00e600;
      color: white;
    }

	.nav-link {
		font-family: arial;
		font-size: 20px;
		text-decoration: none; 
		float : right;
	}

	.judul {
		text-align: center;
		color: #2E5A1C;
		font-size: 40px;
		margin-top: 30px;
	}

	</style>
  </head>
  <body background="backgroundsayur.png">

	<nav class="navbar navbar-expand-lg navbar-dark"style="background-color: #006600;">
	<ul class="navbar-nav mr-auto">
		<a class="navbar-brand" href="landingpage.php">
			<img src="PUTIH.png" width="150" height="30" alt="">
		</a>
		</ul>
		<ul class="navbar-nav form-inline my-2 my-lg-0">
		<li class="nav-item">
			<a class="nav-link" href="editprofileCus.php" >Edit Profile</a>
			</li>
			<li class="nav-item">
			<a class="nav-link" href="landingpage.php">Log Out</a>
			</li>
			</ul>
	</nav>


	<div class="container">
	<h1 class="judul">EDIT SAYUR</h1>
  	<hr>
	<div class="row">
      <div class="col-md-3">
        <div class="text-center">
          <img src="//placehold.it/100" alt="foto">
          <h6>Upload a different photo...</h6>
          
          <input type="file" class="form-control">
        </div>
      </div>
      

      <div class="col-md-9 personal-info">
        <form class="form-horizontal" role="form" method="POST" action="update.php">
		<?php
		include "config.php";
		$id = $_POST['update'];
		$data = mysqli_query($conn, "SELECT * FROM sayur WHERE id='$id'");
		$hasil = mysqli_fetch_assoc($data);
		?>
          <div class="form-group">
            <label class="col-lg-3 control-label">Nama Sayur:</label>
            <div class="col-lg-8">
              <input class="form-control" name="jenis" type="text" value="<?php echo $hasil['jenis']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Alamat: </label>
            <div class="col-lg-8">
              <input class="form-control" name="alamat" type="text" value="<?php echo $hasil['alamat']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Telepon:</label>
            <div class="col-lg-8">
              <input class="form-control" name="telepon" type="bigint" value="<?php echo $hasil['telepon']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Luas:</label>
            <div class="col-lg-8">
              <input class="form-control" name="luas" type="text" value="<?php echo $hasil['luas']?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Berat:</label>
            <div class="col-lg-8">
              <input class="form-control" name="berat" type="text" value="<?php echo $hasil['berat']?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="submit" name="submit" class="btn btn-primary" value="update">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<hr>
	
<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>