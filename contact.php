<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact Us</title>
    <style>
    * {
      box-sizing: border-box;
    }  

    body {
      margin: 0;
      font-family: Arial, Helvetica, sans-serif;
      background : white;
    }

    .topnav {
      overflow: hidden;
      background-color:#006600;
    }
    
    .topnav a {
      float: left;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      font-size: 17px;
      text-decoration : none;
    }
    
    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }
    .topnav a.active {
      background-color: #00e600;
      color: white;
    }
    .dropbtn {
      background-color: #006600;
      color: white;
      padding: 14px 16px;

      font-size: 17px;
      border: none;
    }
    .dropdown {
      position: relative;
      display: inline-block;
    }
    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f1f1f1;
      min-width: 150px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }
    .dropdown-content a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
    }
    .dropdown-content a:hover {background-color: #ddd;}

    .dropdown:hover .dropdown-content {display: block;}

    .dropdown:hover .dropbtn {background-color: #3e8e41;}

    input[type=text], select, textarea {
      width: 100%;
      padding: 12px;
      border: 1px solid #ccc;
      margin-top: 6px;
      margin-bottom: 16px;
      resize: vertical;
    }

    button {
      background-color: #4CAF50;
      color: white;
      padding: 12px 20px;
      border: none;
      cursor: pointer;
    }

    button:hover {
      background-color: #45a049;
    }

    
    .container {
      border-radius: 5px;
      background-color: #f2f2f2;
      padding: 10px;
    }

    
    .column {
      float: left;
      width: 50%;
      margin-top: 6px;
      padding: 20px;
    }
    
    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    </style>
  </head>
    <body>
    <div class="topnav">
      <a href="landingpage.php">Home</a>
      <a href="aboutus.php">About Us</a>
      <a class="active" href="contact.php">Contact Us</a>
      <a href="signup.php">Sign Up</a>
      <div class="dropdown">
        <button class="dropbtn">Login</button>
        <div class="dropdown-content">
        <a href="Login.php">Petani</a>
        <a href="Login.php">Customer</a>
    </div>
    </div>
    <div class="container">
      <div style="text-align:center">
        <h2>Hubungi Kami</h2>
        <p>kami di sini untuk membantu dan menjawab pertanyaan apa pun yang Anda miliki:)</p>
      </div>
      <div class="row">
        <div class="column">
          <br><br>
          <img src="petani.jpg" style="width:80%">
        </div>
        <div class="column">
          <form action="" method="POST">
            <label for="fname">Nama Depan</label>
            <input type="text" id="fname" name="firstname" placeholder="Nama anda..">
            <label for="lname">Nama Akhir</label>
            <input type="text" id="lname" name="lastname" placeholder="Nama akhir anda..">
            <label for="lname">Daerah</label>
            <input type="text" id="daerah" name="daerah" placeholder="Daerah Anda..">
            <label for="pesan">Apa Yang Dapat Kami Bantu?</label>
            <textarea id="pesan" name="pesan" placeholder="tuliskan sesuatu.." style="height:170px"></textarea>
            <button type="submit" name="submit">Submit</button>
            <p id="notif"></p>
            
          </form>
        </div>
      </div>
    </div>
    </body>
</html>

<?php
include 'config.php';
if(isset($_POST['submit'])){
  $fname = $_POST['firstname'];
  $lname = $_POST['lastname'];
  $daerah = $_POST['daerah'];
  $pesan = $_POST['pesan'];

  $query = "INSERT INTO `contact`(`nama_depan`, `nama_belakang`, `daerah`, `pesan`) VALUES ('$fname','$lname','$daerah','$pesan')";
  $result = mysqli_query($conn,$query);
  if($result){
      echo "Pesan Terkirim";
      } else {
        echo "Pesan Dibatalkan";
      }
  }
?>