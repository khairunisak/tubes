<?php 
	include "config.php"
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.min.css" />

    <title>See More Sayur</title>
	<style media="screen">

  body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

  * {
    box-sizing: border-box;
  }

    .nav-item a:hover {
      background-color: #ddd;
      color: black;
    }
    .topnav a.active {
      background-color: #00e600;
      color: white;
    }

	.nav-link {
		font-family: arial;
		font-size: 20px;
		text-decoration: none; 
		float : right;
	}

	.judul {
		text-align: center;
		color: #2E5A1C;
		font-size: 40px;
		margin-top: 30px;
	}

	</style>
  </head>
  <body background="backgroundsayur.png">

<nav class="navbar navbar-expand-lg navbar-dark"style="background-color: #006600;">
<ul class="navbar-nav mr-auto">
      <a class="navbar-brand" href="home.php">
        <img src="PUTIH.png" width="150" height="30" alt="">
      </a>
    </ul>
    <ul class="navbar-nav form-inline my-2 my-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="editprofileCus.php" >Edit Profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="landingpage.php">Log Out</a>
        </li>
		</ul>
  </nav>

	<h1 class="judul">SAYURKU</h1>
    
    <div class="row">
    <?php
				include "config.php";

				$data = mysqli_query($conn, "SELECT * FROM sayur");
				while($hasil = mysqli_fetch_assoc($data)){
          ?>
        <div class="col-4" style="padding: 0px 50px;">
        <div class="card" style="width: 18rem;">
        <img src="chilli.png" class="card-img-top" alt="java">
          <div class="card-body">
            <h5 class="card-title"><b><?php echo $hasil['jenis']?></b></h5>
            <label class="card-text">
            <img width="8%" src="placeholder.png"><b> <?php echo $hasil['alamat']?></b>
            <br><br>
            <img width="7%" src="phone.png"><b> <?php echo $hasil['telepon']?></b>
            <br><br>
            <img width="8%" src="grass.png"><b> <?php echo $hasil['luas']?> Ha</b>
            <br><br>
            <img width="8%" src="scale.png"><b> <?php echo $hasil['berat']?> ton</b>
            </label>
            <form action="update_sayur.php" method="POST">
            <button type="submit" name="delete" style="border: none; background:none" value="<?php
            echo $hasil['id']
            ?>"><img style="width: 29px;"src="Vector.png"></button>
			    	</form>
            <form action="delete_sayur.php" method="POST">
            <button type="submit" name="delete" style="border: none; background:none" onclick="myFunction()" value="<?php
            echo $hasil['id']
            ?>"><img style="width: 29px;"src="delete.png"></button>
			    	</form>
          </div>
            <div class="card-footer">
            <small class="text-muted">Last updated 3 mins ago</small>
            </div>
        </div>
      </div>

      <?php
				}
			?>
</div>
<script>
function myFunction() {
  confirm("Anda yakin ingin menghapus?");
}
</script>

<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>