<!DOCTYPE html>
<html>
<head>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
     body {
     	margin: 0;
      font-family: Arial, Helvetica, sans-serif;
      color: white;
      background-image: url("sawah 2.jpeg");
      
    }
    .topnav {
      overflow: hidden;
      background-color:#006600;
    }
    .topnav a {
      float: left;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }
    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }
    .topnav a.active {
      background-color: #00e600;
      color: white;
    }
    .dropbtn {
      background-color: #006600;
      color: white;
      padding: 16px;
      font-size: 16px;
      border: none;
    }
    .dropdown {
      position: relative;
      display: inline-block;
    }
    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f1f1f1;
      min-width: 150px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }
    .dropdown-content a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
    }
    .dropdown-content a:hover {background-color: #ddd;}

    .dropdown:hover .dropdown-content {display: block;}

    .dropdown:hover .dropbtn {background-color: #3e8e41;}

    .bg-image {
      /* The image used */
      background-image: url("sawah 2.jpeg");
      
      /* Full height */
      height: 100%; 
      
      /* Center and scale the image nicely */
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
    }
    .sosmed   {
    	width: 50px;
    }
    a {
    	color: white;
    }
</style>
	<title>AboutUs</title>
</head>
<body>
	<div class="topnav">
      <a class="active" href="landingpage.php">PETANIKU</a>
       </div>
       <section>
       	<img style="width: 100%">
       </section>
       <div class="bg-image"></div>
       <<center>
       <h1>PETANIKU</h1>
       <h3>Petaniku adalah sebuah platform dalam bentuk website yang menyediakan informasi mengenai petani yang ingin menjual produknya dalam skala besar ke customer. website ini akan membantu petani untuk menemukan customernya dan customer pun akan dibantu dalam mencari petani maupun hasil pertanian yang berkualitas dan terpercaya. Melalui website ini, Kami menyediakan 3 fitur utama yaitu fitur berasku, buahku dan sayurku. semua fitur tersebut dikategorikan berdasarkan jenisnya.
       Melalui slogan kami yaitu Go Green, Go local kami berharap petani lokal indonesia dapat lebih maju dan modren serta berkontribusi dalam program swasembada pangan pemerintah.</h3>

       <br>

       <table class="sosmed">
       	<tr>
       		<td><img src="instagram.png" href="https://www.instagram.com/?hl=id" class="sosmed"></td>
       		<td><img src="twitter.png" href="https://twitter.com/login?lang=id" class="sosmed"></td>
       		<td><img src="Facebook-Logo.png" href="https://www.facebook.com/" class="sosmed"></td>
       	</tr>
       	<tr>
       		<td><a href="https://www.instagram.com/?hl=id">Instagram PETANIKU</a></td>
       		<td><a href="https://twitter.com/login?lang=id">Twitter PETANIKU</a></td>
       		<td><a href="https://www.facebook.com/">Facebook PETANIKU</a></td>
       	</tr>
       </table>
   </center>

   <div></div>

</body>
</html>