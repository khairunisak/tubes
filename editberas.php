<?php

	include("config.php");

	$query = mysqli_query($conn, "SELECT * FROM berasku");
	$hasil = mysqli_fetch_array($query);
	
?>

<html>
<head>
	<title>Input Product</title>
	<script type="text/javascript">
		function previewFile() {
			var preview = document.getElementById("previewimage");
			var file    = document.getElementById("file-input").files[0];
			var reader  = new FileReader();
			reader.addEventListener("load", function () {
				preview.src = reader.result;
			}, false);
			if (file) {
				reader.readAsDataURL(file);
			}
		}
	</script>

	<style>
		*{
		margin: 0;
		padding: 0;
	}
	body{
		background-repeat: no-repeat;
		width: 100%;
		height: 100%
		background-size:100%;
	}
	#output_image {
		width: 100%;
	}
	.ContainerNavbar {
		background-color: #2E5A1C;
		list-style: none;
		position: relative;
		display: inline-block;
		width: 100%;
		height : 6.5%;
	}

	.NavbarKiri {
		float : left;
		width : 20%;
		margin-left: 25px;
		margin-top: 8px;
	}

	.LogoNavbar {
		width : 200px;
	}

	.NavbarKanan {
		float: right;
		width : 20%;
		height : 40%;
		margin-top: 20px;
	}

	.TulisanNavbarLogout {
		font-family: arial;
		color: white;
		font-size: 20px;
		text-decoration: none;
		float : right;
	}

	.identitasproduk {
		color: #2E5A1C;
		font-size: 40px;
		margin-top: 50px;
	}

	.containerimg {
		width: 200px;
		height: 200px;
		margin-top: 10px;
	}

	.image-upload>input {
		display: none;
	}

	.image-upload {
		-webkit-filter : drop-shadow(0px 3px 3px #000000);
		filter : drop-shadow(0px 3px 3px #000000);
	}

	.img {
		width: 150px;
		height: 150px;
		margin-top: 25px;
	}

	.table {
		margin-top: 40px;
	}

	.containertombol {
		margin-top: 30px;
		width: 250px;
		height: 20px;
	}

	.submit {
		width: 70px;
		height: 20px;
		float: left;
		background-color: #2E5A1C;
		color: white;
	}

	.button {
		width: 70px;
		height: 20px;
		float: right;
		background-color: #2E5A1C;	
		color: white;
	}
	#wrapper {
		text-align: center;
		margin: 0 auto;
		padding: 0px;
		width: 500px;
	}

	</style>
</head>
<body>
	<div class = "ContainerNavbar">
		<div class = "NavbarKiri">
			<a href = "home.php"> <img src = "PUTIH.png" class = "LogoNavbar"> </a>
		</div>

		<div class = "NavbarKanan">
			<div style = "width : 100px; float : right; margin-right: 30px;">
			<a href = "logoutku.php" class = "TulisanNavbarLogout"> Log Out </a>
			</div>
		</div>
	</div>

	<center>
	<div class="containerh1">
		<h1 class="identitasproduk">PERBAHARUI PRODUK</h1>
	</div>

	<form action="updateberas.php" method="POST" enctype="multipart/form-data">

		<input type="text" name="id" value=" <?= $_POST['id']; ?>" hidden>

		<div class="containerimg">

		<div style = "margin-left: 35px;" class="image-upload">
		<center>
			<label style = "cursor: pointer;" for="file-input">
				<img id = "previewimage" src = "tambahproduct.png" class="containerimg">
			</label>
		</center>
			<input name = "gambar" id = "file-input" type = "file" onchange = "previewFile()" required>
		</div>

		</div>

	<table class="table">
		<tr>
			<td><h3>Nama Produk</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="nama" value="<?php echo $hasil[2]; ?>"></h3></td>
		</tr>
		<tr>
			<td><h3>Luas Lahan</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="lahan" value="<?php echo $hasil[3]; ?>">Ha</h3></td>
		</tr>
		<tr>
			<td><h3>Produk yang Tersedia</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="berat" value="<?php echo $hasil[4]; ?>">Ton</h3></td>
		</tr>
		<tr>
			<td><h3>Lokasi</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="lokasi" value="<?php echo $hasil[5]; ?>"></h3></td>
		</tr>
		<tr>
			<td><h3>No Telephone yang Dapat Dihubungi</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="nohp" value="<?php echo $hasil[6]; ?>"></h3></td>
		</tr>
	</table>

	<div class="containertombol">
		<div>
			<input type="submit" name="submit" value="SUBMIT" class="submit">
		</div>
		
		<div>
			<input type="button" name="clear" value="CLEAR" class="button">
		</div>
		
	</div>
	</form>
	
	</center>
<?php
	include("updateberas.php");
?>

</body>
</html>