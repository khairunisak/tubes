<!DOCTYPE html>

<?php

session_start();

?>

<html>
<head>
	<title>Input Product</title>
	<style>
		*{
		margin: 0;
		padding: 0;
	}
	body{
		background-repeat: no-repeat;
		width: 100%;
		height: 100%
		background-size:100%;
	}

	.ContainerNavbar {
		background-color: #2E5A1C;
		list-style: none;
		position: relative;
		display: inline-block;
		width: 100%;
		height : 6.5%;
	}

	.NavbarKiri {
		float : left;
		width : 20%;
		margin-left: 25px;
		margin-top: 8px;
	}

	.LogoNavbar {
		width : 200px;
	}

	.NavbarKanan {
		float: right;
		width : 20%;
		height : 40%;
		margin-top: 20px;
	}

	.TulisanNavbarLogout {
		font-family: arial;
		color: white;
		font-size: 20px;
		text-decoration: none;
		float : right;
	}

	.identitasproduk {
		color: #2E5A1C;
		font-size: 40px;
		margin-top: 50px;
	}

	.containerimg {
		width: 200px;
		height: 200px;
		background: #C4C4C4;
		margin-top: 35px;
	}

	.img {
		width: 150px;
		height: 150px;
		margin-top: 25px;
	}

	.table {
		margin-top: 20px;
	}

	.containertombol {
		margin-top: 30px;
		width: 250px;
		height: 20px;
	}

	.submit {
		width: 70px;
		height: 20px;
		float: left;
		background-color: #2E5A1C;
		color: white;
	}

	.button {
		width: 70px;
		height: 20px;
		float: right;
		background-color: #2E5A1C;	
		color: white;
	}


	</style>
</head>
<body>
	<div class = "ContainerNavbar">
		<div class = "NavbarKiri">
			<a href = "home.php"> <img src = "PUTIH.png" class = "LogoNavbar"> </a>
		</div>

		<div class = "NavbarKanan">
			<div style = "width : 100px; float : right; margin-right: 30px;">
			<a href = "logoutku.php" class = "TulisanNavbarLogout"> Log Out </a>
			</div>
		</div>
	</div>

	<center>
	<div class="containerh1">
		<h1 class="identitasproduk">IDENTITAS PRODUK</h1>
	</div>

	<form action="tambahbuahku.php" method="POST" enctype="multipart/form-data">

		<input type="text" name="IDupdate" value="" hidden>

		<div class="containerimg">
		<label for="uploadgambar" style="display: inline-block; width: 80%; height: 80%; background-image: url(tambahproduct.png); margin-top: 20px; background-size: 100% 100%; cursor: pointer;">

		</label>
			<input id = "uploadgambar" type="file" name="gambar" value="Pilih Gambar" style="display: none;">
		</div>

	<table class="table">
		<tr>
			<td><h3>Nama Produk</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="nama" value="<?php echo $_SESSION['nama']; ?>"></h3></td>
		</tr>
		<tr>
			<td><h3>Luas Lahan</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="lahan" value="<?php echo $_SESSION['lahan']; ?>">Ha</h3></td>
		</tr>
		<tr>
			<td><h3>Produk yang Tersedia</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="berat" value="<?php echo $_SESSION['berat']; ?>">Ton</h3></td>
		</tr>
		<tr>
			<td><h3>Lokasi</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="lokasi" value="<?php echo $_SESSION['lokasi']; ?>"></h3></td>
		</tr>
		<tr>
			<td><h3>No Telephone yang Dapat Dihubungi</h3></td>
			<td><h3>:</h3></td>
			<td><h3><input type="text" name="nohp" value="<?php echo $_SESSION['nohp']; ?>"></h3></td>
		</tr>
	</table>

	<div class="containertombol">
		<div>
			<input type="submit" name="submit" value="SUBMIT" class="submit">
		</div>
		
		<div>
			<input type="button" name="clear" value="CLEAR" class="button">
		</div>
		
	</div>
	</form>
	
	</center>

</body>
</html>